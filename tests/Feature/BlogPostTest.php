<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BlogPostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function test()
    {

        $user = User::factory('App\User')->create();

        $this->actingAs($user);

        $response = $this->get('blog/create/post');
        $response->assertStatus(200);
        
    }

}