<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() // Requires logging in to access these pages.
{
    Route::get('/', [\App\Http\Controllers\BlogPostController::class, 'index']);
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/blog', [\App\Http\Controllers\BlogPostController::class, 'index']);
    Route::get('/blog/{blogPost}', [\App\Http\Controllers\BlogPostController::class, 'show']);
    Route::get('/blog/create/post', [\App\Http\Controllers\BlogPostController::class, 'create']); //shows create post form
    Route::post('/blog/create/post', [\App\Http\Controllers\BlogPostController::class, 'store']); //saves the created post to the databse
    Route::get('/blog/{blogPost}/edit', [\App\Http\Controllers\BlogPostController::class, 'edit']); //shows edit post form
    Route::put('/blog/{blogPost}/edit', [\App\Http\Controllers\BlogPostController::class, 'update']); //commits edited post to the database 
    Route::delete('/blog/{blogPost}', [\App\Http\Controllers\BlogPostController::class, 'destroy']); //deletes post from the database
    Route::get('/admin', [\App\Http\Controllers\BlogPostController::class, 'admin']);  // shows the admin panel
    Route::get('/admin/user/{user}', [\App\Http\Controllers\BlogPostController::class, 'edituser']); // shows the user editing panel (for administrators only)
    Route::delete('/admin', [\App\Http\Controllers\BlogPostController::class, 'deleteuser']); //deletes the selected user from the database
    Route::put('/admin/user/{user}', [\App\Http\Controllers\BlogPostController::class, 'updateuser']); // uploads the changes made to the user to the database
});