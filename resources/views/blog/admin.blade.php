
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="mx-auto col-12 pt-2">
                <div class="col-2">
                </div>
                <div class="col-8 text-center">
                    <h1 class="display-one mt-3 mb-3">Currently registered users:</h1>
                </div>
                <div class="w-100 row">
                    @forelse($users->sortBy('permission_level')->except(Auth::id()) as $user)
                   <ul class="col-8 mx-auto my-auto p-0">

                       <li class="my-2 ListItemCustom w-100">
                            <div class="PostButtonCustom BorderCustom w-100 row">
                                <div class=" col-4 my-auto overflow-hidden font-weight-bold">
                                <h3 class="text-center my-auto">{{ ucfirst($user->name) }}</h3>
                                </div>
                                    @if($user->permission_level > Auth::user()->permission_level)
                                        <div class="col-3 h-50 my-auto d-flex justify-content-center align-items-center">
                                            <form id="delete-frm" action="" class="w-100 h-100" method="POST">
                                                @method('DELETE')
                                                @csrf
                                                <button class="DeleteButtonCustom ButtonCustom font-weight-light w-100 h-100" onclick="return confirm('Are you sure?')">Delete user</button>
                                                <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}" >
                                            </form>
                                        </div>

                                        <a class="col-1"></a>

                                        <a class="ButtonCustom PostButtonCustom HoverButton col-3 h-50 my-auto
                                        d-flex justify-content-center align-items-center font-weight-light" 
                                        href="/admin/user/{{ $user->id }}">Change user info</a>
                                    @else
                                    <div class="col-3 h-50 my-auto pt-2 d-flex justify-content-center align-items-center">
                                        <h5>Administrator</h5>
                                    </div>
                                    @endif
                                <a class="col-1"></a>
                            </div>
                       </li>
                   </ul>

                    @empty
                </div>
               <p class="text">No other registered users!</p>
               @endforelse
           </div>
        </div>
    </div>
@endsection