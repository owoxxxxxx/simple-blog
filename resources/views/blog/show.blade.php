@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <a href="{{ url()->previous() }}" class="btn ButtonCustom btn-m mt-3 mb-3">Go back</a>

                <div class="row">
                    <div class="col-6 p-4 CustomHr1">
                        <h1 class="display-one">{{ ucfirst($post->title) }}</h1>
                        <p>{!! $post->body !!}</p>  
                    </div>

                    <div class="col-5 p-4">
                        <h3>Tasks:</h3>
                        @forelse($tasks as $task)
                            <li class="LiElementCustom row mx-1 mt-2 rounded" name="task" value={{ $task->is_checked }}>
                                <div class="col-12 p-2 px-2 d-flex justify-content-start align-items-center">
                                <div class="px-2 ButtonCustom w-100 h-100 pt-2" value="">
                                    {{ $task->name }}
                                </div>
                            </li>
                        @empty
                            <p>No tasks :(</p>
                        @endforelse
                    </div> 
                </div>

                <hr>
                <div id="map" class="mt-4"></div>
                <script>
                    updatetaskstatus();
                    function updatetaskstatus()
                    {
                        var tasks = document.getElementsByName("task");
                        var taskCount = tasks.length;
                        for(id = 0; id < taskCount; id++)
                        {
                            if (tasks[id].value == 1)
                            {
                                tasks[id].style.background = 'var(--success-green-l)';
                            }
                            else
                            {
                                tasks[id].style.background = 'rgba(0,0,0,0)';
                            }
                        }
                    }

                    var lp = new locationPicker('map', {
                    setCurrentPosition: true,
                    }, {
                        zoom: 10,
                        draggable: false,
                        keyboardShortcuts: false,
                        center: { lat: {{ $post->latitude }}, lng: {{ $post->longitude }} }
                    });
                </script>
                <hr>
                @if(Auth::check() && (Auth::user()->id == $post->user_id) || Auth::user()->permission_level <= 3 && Auth::user()->permission_level < $post->user->permission_level)
                <a href="/blog/{{ $post->id }}/edit" class="btn ButtonCustom btn-m">Edit Post</a>
                <br><br>
                <form id="delete-frm" class="" action="" method="POST">
                    @method('DELETE')
                    @csrf
                    <button class="btn ButtonCustom btn-m">Delete Post</button>
                </form>
                @else<p>You cannot edit this post</p>
                @endif
            </div>
        </div>
    </div>
@endsection
