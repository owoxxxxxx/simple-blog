@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 pt-2">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-8 text-center">
                        <h1 class="display-one mt-3 mb-3">Here's a list of posts</h1>
                        <p>Don't bother reading them, it's a waste of time</p>
                    </div>
                    <div class="col-2 text-center">
                        <a href="/blog/create/post" class="btn ButtonCustom btn-m" dusk="createPost">Create a new post</a>
                    </div>
                </div>                
                @forelse($posts as $post)
                    <ul class="col-8 mx-auto pl-0">
                        <li class="HoverButton ListItemCustom w-100">
                            <a class="HoverButton d-flex justify-content-center" href="./blog/{{ $post->id }}">
                                <button class="ButtonCustom PostButtonCustom w-100 row">
                                    <div class="ButtonSec1Custom col-xs-6 d-flex justify-content-center font-weight-bold px-3">{{ ucfirst($post->title) }}</div>
                                    <div class="ButtonSec3Custom col-xs-6 d-flex justify-content-center align-items-center font-weight-light">Created by:<br>{{ User::where('id',$post->user_id)->first()->name }}</div>
                                    <div class="ButtonSec2Custom col-xs-6 d-flex justify-content-center align-items-center font-weight-light">{{ ucfirst($post->updated_at) }}</div>
                                </button>
                            </a>
                        </li>
                    </ul>
                @empty
                    <p class="text-center mt-4">No Great posts available</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
