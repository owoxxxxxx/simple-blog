
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header CardHeaderCustom">{{ __('Change user info') }}</div>

                <div class="card-body">
                    <form method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn ButtonCustom">
                                    {{ __('Save changes') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-8 text-center">
                    <h1 class="display-one mt-3 mb-3">Posts created by the user:</h1>
                </div>

                <div class="col-12 pt-2">              
                    @forelse($posts as $post)
                        <ul class="col-8 mx-auto pl-0">
                            <li class="HoverButton ListItemCustom w-100">
                                <a class="HoverButton d-flex justify-content-center" href="/blog/{{ $post->id }}">
                                    <button class="ButtonCustom PostButtonCustom w-100 row">
                                        <div class="ButtonSec1Custom col-xs-6 d-flex justify-content-center font-weight-bold px-3">{{ ucfirst($post->title) }}</div>
                                        <div class="ButtonSec2Custom col-xs-6 d-flex justify-content-center align-items-center font-weight-light">{{ ucfirst($post->updated_at) }}</div>
                                    </button>
                                </a>
                            </li>
                        </ul>
                    @empty
                        <p class="text-center mt-4">This user hasn't created any posts :(</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
